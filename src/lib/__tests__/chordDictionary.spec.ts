import { describe, it, expect } from "vitest";

import * as cd from "../chordDictionary";

describe("Basic", () => {
  describe("lookUpChordDef", () => {
    it("C", () => {
      expect(cd.lookUpChordDef("C")).toEqual([
        [0, 8, 8, 8, 0],
        [3, 0, 0, 0, 3],
      ]);
    });
    it("Flat and Sharp", () => {
      const chordBb = [
        [1, 0, 0, 1, 0],
        [2, 0, 2, 0, 0],
        [3, 3, 0, 0, 0],
      ];
      expect(cd.lookUpChordDef("A#")).toEqual(chordBb);
      expect(cd.lookUpChordDef("Bb")).toEqual(chordBb);
    });
    it("NC", () => {
      const chordNc = [[0, 9, 9, 9, 9]];
      expect(cd.lookUpChordDef("NC")).toEqual(chordNc);
      expect(cd.lookUpChordDef("N/C")).toEqual(chordNc);
    });
    it("Aug", () => {
      const chordEaug = [
        [0, 1, 8, 8, 0],
        [4, 0, 0, 0, 4],
      ];
      expect(cd.lookUpChordDef("E+")).toEqual(chordEaug);
      expect(cd.lookUpChordDef("Eaug")).toEqual(chordEaug);
    });
    it("Undefined", () => {
      expect(cd.lookUpChordDef("Undefined")).toBeUndefined();
    });
  });
  describe("nameIsAChord", () => {
    it("Defined", () => {
      expect(cd.nameIsAChord("Cm")).toBeTruthy();
      expect(cd.nameIsAChord("Cm^2")).toBeTruthy();
    });
    it("Undefined", () => {
      expect(cd.nameIsAChord("Cm^3")).toBeFalsy();
    });
  });
  it("removeBarres", () => {
    expect(
      cd.removeBarres([
        [3, 8, 3, 0, 0],
        [2, 3, 0, 0, 2],
        [0, 1, 0, 8, 3],
      ])
    ).toEqual([
      [0, 0, 0, 8, 0],
      [2, 0, 0, 0, 2],
      [3, 8, 3, 0, 0],
    ]);
  });
  describe("lookUpNames", () => {
    it("single", () => {
      const chordD = cd.lookUpChordDef("D");
      expect(chordD).toBeDefined();
      if (chordD !== undefined) {
        expect(cd.lookUpNames(chordD)).toEqual(["D"]);
      }
    });
    it("multiple", () => {
      const chordAm7 = cd.lookUpChordDef("Am7");
      expect(chordAm7).toBeDefined();
      if (chordAm7 !== undefined) {
        expect(cd.lookUpNames(chordAm7)).toEqual(["Am7", "C6"]);
      }
    });
    it("sharp and flat", () => {
      const chordEb = cd.lookUpChordDef("Eb");
      expect(chordEb).toBeDefined();
      if (chordEb !== undefined) {
        expect(cd.lookUpNames(chordEb)).toEqual(["D#", "Eb"]);
      }
    });
  });
  it("makeLeftHanded", () => {
    const leftHandedChordC = [
      [0, 0, 8, 8, 8],
      [3, 3, 0, 0, 0],
    ];
    const chordC = cd.lookUpChordDef("C");
    expect(chordC).toBeDefined();
    if (chordC !== undefined) {
      expect(cd.makeLeftHanded(chordC)).toEqual(leftHandedChordC);
    }
  });
  describe("lookUpNames", () => {
    it("single", () => {
      const chordD = cd.lookUpChordDef("D");
      expect(chordD).toBeDefined();
      if (chordD !== undefined) {
        expect(cd.lookUpNames(chordD)).toEqual(["D"]);
      }
    });
    it("multiple", () => {
      const chordAm7 = cd.lookUpChordDef("Am7");
      expect(chordAm7).toBeDefined();
      if (chordAm7 !== undefined) {
        expect(cd.lookUpNames(chordAm7)).toEqual(["Am7", "C6"]);
      }
    });
    it("sharp and flat", () => {
      const chordEb = cd.lookUpChordDef("Eb");
      expect(chordEb).toBeDefined();
      if (chordEb !== undefined) {
        expect(cd.lookUpNames(chordEb)).toEqual(["D#", "Eb"]);
      }
    });
  });
  it("makeLeftHanded", () => {
    const leftHandedChordC = [
      [0, 0, 8, 8, 8],
      [3, 3, 0, 0, 0],
    ];
    const chordC = cd.lookUpChordDef("C");
    expect(chordC).toBeDefined();
    if (chordC !== undefined) {
      expect(cd.makeLeftHanded(chordC)).toEqual(leftHandedChordC);
    }
  });
  it("uniqueChordDefs", () => {
    expect(cd.uniqueChordDefs()).toBe(cd.uniqueChordDefs());
    // check that all chords exist in the look ups
    cd.uniqueChordDefs().forEach((c) => {
      c.names.forEach((name) => {
        expect(cd.nameIsAChord(name)).toBeTruthy();
        // some chords are intentionally different as they're "normalised", so only sanity check
        if (name === "C") {
          const chordC = [
            [0, 8, 8, 8, 0],
            [3, 0, 0, 0, 8],
          ];
          expect(c.chordDef).toEqual(chordC);
        }
      });
    });
  });
  it("equals", () => {
    const chordC = [
      [0, 8, 8, 8, 0],
      [3, 0, 0, 0, 8],
    ];
    expect(cd.chordDefEqual(cd.normaliseChordDef(chordC), chordC)).toBeTruthy();
  });
});
