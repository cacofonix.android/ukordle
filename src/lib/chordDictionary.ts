export type ChordDefinition = Array<Array<number>>;
export type Dictionary = {
  [key: string]: ChordDefinition;
};
// numbers are finger positions, with special cases of:
// 8 is no finger
// 9 is muted hit
const dictionary: Dictionary = {
  A: [
    [0, 0, 0, 8, 8],
    [1, 0, 1, 0, 0],
    [2, 2, 0, 0, 0],
  ],
  Am: [
    [0, 0, 8, 8, 8],
    [2, 2, 0, 0, 0],
  ],
  "Am/C": [
    [0, 0, 8, 8, 0],
    [2, 2, 0, 0, 0],
    [3, 0, 0, 0, 4],
  ],
  Aaug: [
    [1, 1, 1, 1, 1],
    [2, 2, 0, 0, 0],
    [4, 0, 0, 0, 4],
  ],
  Adim: [
    [2, 1, 0, 2, 0],
    [3, 0, 3, 0, 4],
  ],
  A7: [
    [0, 8, 0, 8, 8],
    [1, 0, 1, 0, 0],
  ],
  A7aug5: [
    [0, 8, 0, 0, 8],
    [1, 0, 1, 1, 0],
  ],
  Am7: [[0, 8, 8, 8, 8]],
  Ammaj7: [
    [0, 0, 8, 8, 8],
    [1, 1, 0, 0, 0],
  ],
  Amaj7: [
    [0, 0, 0, 8, 8],
    [1, 1, 2, 0, 0],
  ],
  A6: [
    [0, 0, 0, 0, 8],
    [1, 0, 2, 0, 0],
    [2, 1, 0, 3, 0],
  ],
  Am6: [
    [0, 0, 8, 0, 8],
    [2, 1, 0, 3, 0],
  ],
  Aadd9: [
    [0, 0, 0, 8, 0],
    [1, 0, 1, 0, 0],
    [2, 2, 0, 0, 4],
  ],
  Am9: [
    [0, 0, 8, 8, 0],
    [2, 2, 0, 0, 4],
  ],
  A9: [
    [0, 8, 0, 8, 0],
    [1, 0, 1, 0, 0],
    [2, 0, 0, 0, 3],
  ],
  Asus2: [
    [2, 1, 1, 1, 1],
    [4, 0, 3, 0, 0],
    [5, 0, 0, 4, 0],
  ],
  Asus4: [
    [0, 0, 0, 8, 8],
    [2, 2, 3, 0, 0],
  ],
  A7sus4: [
    [0, 8, 0, 8, 8],
    [2, 0, 3, 0, 0],
  ],
  Bb: [
    [1, 0, 0, 1, 0],
    [2, 0, 2, 0, 0],
    [3, 3, 0, 0, 0],
  ],
  "Bb^2": [
    [1, 1, 1, 1, 1],
    [2, 0, 2, 0, 0],
    [3, 3, 0, 0, 0],
  ],
  "Bb^3": [
    [5, 0, 1, 0, 2],
    [6, 0, 0, 3, 0],
  ],
  Bbdim7: [
    [0, 8, 0, 8, 0],
    [1, 0, 2, 0, 3],
  ],
  Bbm: [[1, 0, 1, 1, 1]],
  "Bbm^2": [
    [1, 1, 1, 1, 1],
    [3, 3, 0, 0, 0],
  ],
  Bbaug: [
    [1, 1, 1, 1, 1],
    [2, 0, 3, 2, 0],
    [3, 4, 0, 0, 0],
  ],
  Bbdim: [
    [0, 0, 0, 8, 0],
    [1, 0, 1, 0, 2],
    [3, 4, 0, 0, 0],
  ],
  Bb7: [
    [1, 1, 1, 1, 1],
    [2, 0, 2, 0, 0],
  ],
  Bbm7: [[1, 1, 1, 1, 1]],
  Bbmaj7: [
    [0, 0, 0, 0, 8],
    [1, 0, 0, 1, 0],
    [2, 0, 2, 0, 0],
    [3, 3, 0, 0, 0],
  ],
  Bb6: [
    [0, 8, 0, 0, 0],
    [1, 0, 0, 1, 1],
    [2, 0, 2, 0, 0],
  ],
  Bbm6: [
    [1, 1, 1, 1, 1],
    [3, 3, 0, 4, 0],
  ],
  Bbadd9: [
    [1, 0, 0, 1, 0],
    [2, 0, 2, 0, 0],
    [3, 3, 0, 0, 4],
  ],
  Bbm9: [
    [1, 1, 1, 1, 1],
    [3, 3, 0, 0, 4],
  ],
  Bb9: [
    [2, 0, 1, 0, 0],
    [3, 2, 0, 0, 3],
    [4, 0, 0, 4, 0],
  ],
  Bbsus2: [
    [0, 0, 8, 0, 0],
    [1, 0, 0, 1, 1],
    [3, 3, 0, 0, 0],
  ],
  Bbsus4: [
    [1, 0, 0, 1, 1],
    [3, 3, 4, 0, 0],
  ],
  Bb7sus4: [
    [1, 1, 1, 1, 1],
    [3, 0, 3, 0, 0],
  ],
  B: [
    [2, 0, 0, 1, 0],
    [3, 0, 2, 0, 0],
    [4, 3, 0, 0, 0],
  ],
  "B^2": [
    [2, 1, 1, 1, 1],
    [3, 0, 2, 0, 0],
    [4, 3, 0, 0, 0],
  ],
  "B^3": [
    [6, 0, 1, 0, 2],
    [7, 0, 0, 3, 0],
  ],
  Bm: [[2, 0, 1, 1, 1]],
  "Bm^2": [
    [2, 1, 1, 1, 1],
    [4, 3, 0, 0, 0],
  ],
  Baug: [
    [2, 1, 1, 1, 1],
    [3, 0, 3, 2, 0],
    [4, 4, 0, 0, 0],
  ],
  Bdim: [
    [1, 0, 0, 8, 0],
    [2, 0, 8, 0, 8],
    [4, 8, 0, 0, 0],
  ],
  B7: [
    [0, 0, 0, 0, 8],
    [2, 0, 0, 1, 0],
    [3, 0, 2, 0, 0],
    [4, 3, 0, 0, 0],
  ],
  "B7^2": [
    [2, 1, 1, 1, 1],
    [3, 0, 2, 0, 0],
  ],
  Bm7: [[2, 1, 1, 1, 1]],
  Bmaj7: [
    [1, 0, 0, 0, 1],
    [2, 0, 0, 2, 0],
    [3, 0, 3, 0, 0],
    [4, 4, 0, 0, 0],
  ],
  B6: [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 3, 2],
    [3, 0, 4, 0, 0],
  ],
  Bm6: [
    [1, 1, 0, 0, 0],
    [2, 0, 2, 3, 4],
  ],
  Badd9: [
    [2, 0, 0, 1, 0],
    [3, 0, 2, 0, 0],
    [4, 3, 0, 0, 4],
  ],
  Bm9: [
    [0, 0, 0, 8, 0],
    [1, 0, 1, 0, 0],
    [2, 2, 0, 0, 3],
  ],
  B9: [
    [3, 0, 1, 0, 0],
    [4, 2, 0, 0, 3],
    [5, 0, 0, 4, 0],
  ],
  Bsus2: [
    [1, 0, 8, 0, 0],
    [2, 0, 0, 8, 8],
    [4, 8, 0, 0, 0],
  ],
  Bsus4: [
    [2, 0, 0, 1, 1],
    [4, 3, 4, 0, 0],
  ],
  B7sus4: [
    [2, 1, 1, 1, 1],
    [4, 0, 3, 0, 0],
  ],
  C: [
    [0, 8, 8, 8, 0],
    [3, 0, 0, 0, 3],
  ],
  "C^2": [
    [3, 1, 1, 1, 1],
    [4, 0, 2, 0, 0],
    [5, 3, 0, 0, 0],
  ],
  Cm: [
    [0, 8, 0, 0, 0],
    [3, 0, 1, 1, 1],
  ],
  "Cm^2": [
    [3, 1, 1, 1, 1],
    [5, 3, 0, 0, 0],
  ],
  Caug: [
    [0, 0, 8, 8, 0],
    [1, 1, 0, 0, 0],
    [3, 0, 0, 0, 4],
  ],
  Cdim: [
    [0, 9, 0, 0, 0],
    [2, 0, 0, 2, 0],
    [3, 0, 1, 0, 3],
  ],
  C7: [
    [0, 8, 8, 8, 0],
    [1, 0, 0, 0, 1],
  ],
  "C7^2": [
    [0, 0, 8, 8, 0],
    [3, 2, 0, 0, 4],
  ],
  Cm7: [[3, 1, 1, 1, 1]],
  Cmaj7: [
    [0, 8, 8, 8, 0],
    [2, 0, 0, 0, 2],
  ],
  C6: [[0, 8, 8, 8, 8]],
  Cm6: [
    [2, 1, 0, 0, 0],
    [3, 0, 2, 3, 4],
  ],
  C5: [
    [0, 8, 8, 0, 0],
    [3, 0, 0, 3, 4],
  ],
  Cadd9: [
    [0, 8, 0, 8, 0],
    [2, 0, 1, 0, 0],
    [3, 0, 0, 0, 3],
  ],
  Cm9: [
    [3, 1, 1, 1, 1],
    [5, 3, 0, 0, 4],
  ],
  C9: [
    [0, 8, 0, 8, 0],
    [1, 0, 0, 0, 1],
    [2, 0, 2, 0, 0],
  ],
  Csus2: [
    [0, 8, 0, 0, 0],
    [2, 0, 1, 0, 0],
    [3, 0, 0, 3, 4],
  ],
  Csus4: [
    [0, 8, 8, 0, 0],
    [1, 0, 0, 1, 0],
    [3, 0, 0, 0, 3],
  ],
  C7sus4: [
    [0, 8, 8, 0, 0],
    [1, 0, 0, 1, 2],
  ],
  Db: [[1, 1, 1, 1, 0]],
  "Db^2": [
    [1, 1, 1, 1, 1],
    [4, 0, 0, 0, 4],
  ],
  Dbm: [
    [0, 0, 0, 8, 0],
    [1, 1, 1, 0, 0],
  ],
  "Dbm^2": [
    [0, 0, 0, 8, 0],
    [1, 1, 1, 0, 0],
    [4, 0, 0, 0, 4],
  ],
  "Dbm^3": [
    [1, 1, 0, 0, 0],
    [4, 0, 4, 4, 4],
  ],
  "Dbm^4": [
    [4, 1, 1, 1, 1],
    [6, 3, 0, 0, 0],
  ],
  Dbaug: [
    [0, 0, 0, 0, 8],
    [1, 0, 2, 3, 0],
    [2, 1, 0, 0, 0],
  ],
  Dbdim: [
    [0, 8, 0, 8, 0],
    [1, 0, 1, 0, 0],
    [4, 0, 0, 0, 4],
  ],
  Dbdim7: [
    [0, 8, 0, 8, 0],
    [1, 0, 2, 0, 3],
  ],
  Db7: [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 0, 2],
  ],
  Dbm7: [
    [0, 0, 0, 8, 0],
    [1, 1, 2, 0, 0],
    [2, 0, 0, 0, 4],
  ],
  Dbmaj7: [
    [1, 1, 1, 1, 1],
    [3, 0, 0, 0, 3],
  ],
  Db6: [[1, 1, 1, 1, 1]],
  Dbm6: [
    [0, 0, 0, 8, 0],
    [1, 1, 2, 0, 3],
  ],
  Dbadd9: [
    [1, 1, 1, 1, 1],
    [3, 0, 3, 0, 0],
    [4, 0, 0, 0, 4],
  ],
  Dbm9: [
    [0, 0, 0, 8, 0],
    [1, 1, 0, 0, 0],
    [3, 0, 3, 0, 0],
    [4, 0, 0, 0, 4],
  ],
  Db9: [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 0, 2],
    [3, 0, 3, 0, 0],
  ],
  Dbsus2: [
    [1, 1, 1, 1, 1],
    [3, 0, 3, 3, 3],
    [4, 0, 0, 4, 4],
  ],
  Dbsus4: [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 2, 0],
    [4, 0, 0, 0, 4],
  ],
  Db7sus4: [
    [1, 1, 2, 0, 0],
    [2, 0, 0, 3, 4],
  ],
  D: [[2, 1, 1, 1, 0]],
  "D^2": [
    [0, 0, 0, 0, 8],
    [2, 1, 2, 3, 0],
  ],
  Dm: [
    [0, 0, 0, 0, 8],
    [1, 0, 0, 1, 0],
    [2, 2, 3, 0, 0],
  ],
  Daug: [
    [1, 0, 0, 0, 1],
    [2, 0, 3, 2, 0],
    [3, 4, 0, 0, 0],
  ],
  Ddim: [
    [0, 0, 0, 0, 9],
    [1, 1, 0, 2, 0],
    [2, 0, 3, 0, 0],
  ],
  D7: [
    [0, 0, 8, 0, 8],
    [2, 1, 0, 2, 0],
  ],
  "D7^2": [
    [2, 1, 1, 1, 1],
    [3, 0, 0, 0, 2],
  ],
  Dm7: [
    [1, 0, 0, 1, 0],
    [2, 2, 3, 0, 0],
    [3, 0, 0, 0, 4],
  ],
  Dmaj7: [
    [2, 1, 1, 1, 1],
    [4, 0, 0, 0, 3],
  ],
  D6: [[2, 1, 1, 1, 1]],
  D6add9: [
    [2, 1, 1, 1, 1],
    [4, 0, 3, 0, 0],
  ],
  Dm6: [
    [1, 0, 0, 1, 0],
    [2, 2, 3, 0, 4],
  ],
  Dadd4: [
    [0, 9, 0, 0, 8],
    [2, 0, 1, 2, 0],
  ],
  Dadd9: [
    [2, 1, 1, 1, 1],
    [4, 0, 3, 0, 0],
    [5, 0, 0, 0, 4],
  ],
  Dm9: [
    [0, 0, 0, 8, 0],
    [2, 1, 0, 0, 0],
    [5, 0, 3, 0, 4],
  ],
  D9: [
    [2, 1, 1, 1, 1],
    [3, 0, 0, 0, 2],
    [4, 0, 3, 0, 0],
  ],
  Dsus2: [
    [0, 0, 0, 8, 8],
    [2, 2, 3, 0, 0],
  ],
  Dsus4: [
    [0, 8, 0, 0, 8],
    [2, 0, 1, 0, 0],
    [3, 0, 0, 2, 0],
  ],
  D7sus4: [
    [2, 1, 2, 0, 0],
    [3, 0, 0, 3, 4],
  ],
  Eb: [[3, 1, 1, 1, 0]],
  "Eb^2": [
    [0, 8, 0, 0, 0],
    [1, 0, 0, 0, 1],
    [3, 0, 3, 4, 0],
  ],
  "Eb^3": [
    [1, 0, 0, 0, 1],
    [3, 3, 3, 3, 0],
  ],
  Ebm: [
    [1, 0, 0, 0, 1],
    [2, 0, 0, 2, 0],
    [3, 0, 3, 0, 0],
  ],
  "Ebm^2": [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 2, 0],
    [3, 3, 4, 0, 0],
  ],
  Ebaug: [
    [0, 8, 0, 0, 0],
    [1, 0, 0, 0, 1],
    [2, 0, 2, 3, 0],
  ],
  Ebdim: [
    [0, 0, 0, 0, 8],
    [2, 1, 0, 2, 0],
    [3, 0, 3, 0, 0],
  ],
  Ebdim7: [
    [2, 1, 1, 1, 1],
    [3, 0, 2, 0, 3],
  ],
  Eb7: [
    [3, 1, 1, 1, 1],
    [4, 0, 0, 0, 2],
  ],
  Ebm7: [
    [2, 0, 0, 1, 0],
    [3, 2, 3, 0, 0],
    [4, 0, 0, 0, 4],
  ],
  Ebmaj7: [
    [3, 1, 1, 1, 1],
    [5, 0, 0, 0, 3],
  ],
  Eb6: [[3, 1, 1, 1, 1]],
  Ebm6: [
    [2, 0, 0, 1, 0],
    [3, 2, 3, 0, 4],
  ],
  Ebadd9: [
    [0, 8, 0, 0, 0],
    [1, 0, 0, 1, 1],
    [3, 0, 3, 0, 0],
  ],
  Ebm9: [
    [2, 0, 0, 8, 0],
    [5, 0, 8, 0, 0],
    [6, 8, 0, 0, 8],
  ],
  Eb9: [
    [0, 8, 0, 0, 0],
    [1, 0, 1, 2, 3],
  ],
  Ebsus2: [
    [1, 1, 1, 1, 1],
    [3, 3, 4, 0, 0],
  ],
  Ebsus4: [
    [1, 1, 1, 1, 1],
    [3, 0, 3, 0, 0],
    [4, 0, 0, 4, 0],
  ],
  Eb7sus4: [
    [3, 1, 2, 0, 0],
    [4, 0, 0, 3, 4],
  ],
  E: [
    [0, 0, 0, 8, 0],
    [4, 1, 2, 0, 0],
  ],
  "E^2": [[4, 1, 1, 1, 0]],
  "E^3": [
    [2, 1, 1, 1, 1],
    [4, 3, 3, 3, 0],
  ],
  "E^4": [
    [0, 0, 0, 8, 0],
    [1, 1, 0, 0, 0],
    [2, 0, 0, 0, 2],
    [4, 0, 4, 0, 0],
  ],
  "E^5": [
    [4, 1, 1, 1, 1],
    [7, 0, 0, 0, 4],
  ],
  Em: [
    [0, 8, 0, 0, 0],
    [2, 0, 0, 0, 1],
    [3, 0, 0, 2, 0],
    [4, 0, 3, 0, 0],
  ],
  "Em^2": [
    [0, 0, 0, 0, 0],
    [2, 0, 0, 0, 1],
    [3, 0, 0, 2, 0],
    [4, 3, 4, 0, 0],
  ],
  Eaug: [
    [0, 1, 8, 8, 0],
    [4, 0, 0, 0, 4],
  ],
  Edim: [
    [0, 8, 0, 8, 0],
    [1, 0, 1, 0, 2],
  ],
  E7: [
    [0, 0, 0, 8, 0],
    [1, 1, 0, 0, 0],
    [2, 0, 2, 0, 3],
  ],
  Em7: [
    [0, 8, 0, 8, 0],
    [2, 0, 2, 0, 3],
  ],
  Emaj7: [
    [0, 0, 0, 8, 0],
    [1, 1, 0, 0, 0],
    [2, 0, 0, 0, 2],
    [3, 0, 3, 0, 0],
  ],
  E6: [[4, 1, 1, 1, 1]],
  Em6: [
    [0, 8, 0, 8, 0],
    [1, 0, 1, 0, 0],
    [2, 0, 0, 0, 2],
  ],
  Eadd9: [
    [1, 8, 0, 0, 0],
    [2, 0, 0, 8, 8],
    [4, 0, 8, 0, 0],
  ],
  Em9: [
    [0, 8, 0, 0, 0],
    [2, 0, 0, 1, 1],
    [4, 0, 3, 0, 0],
  ],
  E9: [
    [1, 1, 0, 0, 0],
    [2, 0, 2, 3, 4],
  ],
  Esus2: [
    [2, 0, 0, 1, 1],
    [4, 3, 4, 0, 0],
  ],
  Esus4: [
    [2, 1, 1, 1, 1],
    [4, 0, 3, 0, 0],
    [5, 0, 0, 4, 0],
  ],
  E7sus4: [
    [4, 1, 2, 0, 0],
    [5, 0, 0, 3, 4],
  ],
  F: [
    [0, 0, 8, 0, 8],
    [1, 0, 0, 1, 0],
    [2, 2, 0, 0, 0],
  ],
  "F/C": [
    [0, 0, 8, 0, 0],
    [1, 0, 0, 1, 0],
    [2, 2, 0, 0, 0],
    [3, 0, 0, 0, 4],
  ],
  Fm: [
    [3, 0, 0, 0, 1],
    [4, 0, 0, 2, 0],
    [5, 0, 3, 0, 0],
  ],
  "Fm^2": [
    [0, 0, 8, 0, 0],
    [1, 1, 0, 2, 0],
    [3, 0, 0, 0, 4],
  ],
  Faug: [
    [0, 0, 0, 0, 8],
    [1, 0, 2, 1, 0],
    [2, 3, 0, 0, 0],
  ],
  Fdim: [
    [1, 1, 0, 2, 0],
    [2, 0, 3, 0, 4],
  ],
  F7: [
    [1, 0, 0, 1, 0],
    [2, 2, 0, 0, 0],
    [3, 0, 3, 0, 4],
  ],
  "F7^2": [
    [0, 0, 0, 0, 8],
    [1, 0, 0, 1, 0],
    [2, 2, 0, 0, 0],
    [3, 0, 3, 0, 0],
  ],
  Fm7: [
    [1, 1, 1, 1, 1],
    [3, 0, 3, 0, 4],
  ],
  Fmaj7: [
    [1, 0, 0, 1, 0],
    [2, 2, 0, 0, 0],
    [3, 0, 0, 0, 3],
    [4, 0, 4, 0, 0],
  ],
  F6: [
    [1, 0, 0, 1, 0],
    [2, 2, 3, 0, 0],
    [3, 0, 0, 0, 4],
  ],
  Fm6: [
    [1, 1, 1, 1, 1],
    [2, 0, 2, 0, 0],
    [3, 0, 0, 0, 3],
  ],
  Fadd9: [
    [0, 8, 8, 0, 8],
    [1, 0, 0, 1, 0],
  ],
  Fm9: [
    [0, 8, 0, 0, 0],
    [3, 0, 0, 0, 1],
    [4, 0, 0, 2, 0],
    [5, 0, 3, 0, 0],
  ],
  F9: [
    [2, 1, 0, 0, 0],
    [3, 0, 2, 3, 4],
  ],
  Fsus2: [
    [0, 8, 8, 0, 0],
    [1, 0, 0, 1, 0],
    [3, 0, 0, 0, 3],
  ],
  Fsus4: [
    [0, 0, 8, 0, 0],
    [1, 0, 0, 1, 1],
    [3, 3, 0, 0, 0],
  ],
  F7sus4: [
    [5, 1, 2, 0, 0],
    [6, 0, 0, 3, 4],
  ],
  Gb: [
    [1, 0, 1, 0, 3],
    [2, 0, 0, 2, 0],
  ],
  "Gb^2": [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 2, 0],
    [3, 3, 0, 0, 0],
  ],
  Gbm: [
    [0, 0, 0, 0, 8],
    [1, 0, 1, 0, 0],
    [2, 2, 0, 3, 0],
  ],
  Gbaug: [
    [1, 0, 0, 0, 1],
    [2, 0, 3, 2, 0],
    [3, 4, 0, 0, 0],
  ],
  Gbdim: [
    [2, 1, 0, 2, 0],
    [3, 0, 3, 0, 4],
  ],
  Gbdim7: [
    [2, 1, 1, 1, 1],
    [3, 0, 2, 0, 3],
  ],
  Gb7: [
    [2, 0, 0, 1, 0],
    [3, 2, 0, 0, 0],
    [4, 0, 3, 0, 4],
  ],
  Gbm7: [
    [2, 1, 1, 1, 1],
    [4, 0, 3, 0, 4],
  ],
  Gbmaj7: [
    [2, 0, 0, 1, 0],
    [3, 2, 0, 0, 0],
    [4, 0, 0, 0, 3],
    [5, 0, 4, 0, 0],
  ],
  Gb6: [
    [2, 0, 0, 1, 0],
    [3, 2, 3, 0, 0],
    [4, 0, 0, 0, 4],
  ],
  Gbm6: [
    [1, 0, 1, 0, 0],
    [2, 2, 0, 3, 0],
    [3, 0, 0, 0, 4],
  ],
  Gbadd9: [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 2, 0],
  ],
  Gbm9: [
    [0, 0, 0, 0, 8],
    [1, 1, 0, 0, 0],
    [2, 0, 2, 3, 0],
  ],
  Gb9: [
    [0, 0, 0, 8, 0],
    [1, 1, 2, 0, 3],
  ],
  Gbsus2: [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 2, 0],
    [4, 0, 0, 0, 4],
  ],
  Gbsus4: [
    [1, 0, 8, 0, 0],
    [2, 0, 0, 8, 8],
    [4, 8, 0, 0, 0],
  ],
  Gb7sus4: [
    [6, 1, 2, 0, 0],
    [7, 0, 0, 3, 4],
  ],
  G: [
    [0, 8, 0, 0, 0],
    [2, 0, 1, 0, 2],
    [3, 0, 0, 3, 0],
  ],
  Gm: [
    [0, 8, 0, 0, 0],
    [1, 0, 0, 0, 1],
    [2, 0, 2, 0, 0],
    [3, 0, 0, 3, 0],
  ],
  Gaug: [
    [0, 8, 0, 0, 0],
    [2, 0, 0, 0, 1],
    [3, 0, 2, 3, 0],
  ],
  Gdim: [
    [0, 8, 0, 0, 0],
    [1, 0, 1, 1, 1],
    [3, 0, 0, 3, 0],
  ],
  G7: [
    [0, 8, 0, 0, 0],
    [1, 0, 0, 1, 0],
    [2, 0, 2, 0, 3],
  ],
  Gm7: [
    [0, 8, 0, 0, 0],
    [1, 0, 0, 1, 1],
    [2, 0, 2, 0, 0],
  ],
  Gmaj7: [
    [0, 8, 0, 0, 0],
    [2, 0, 1, 1, 1],
  ],
  G6: [
    [0, 8, 0, 8, 0],
    [2, 0, 1, 0, 2],
  ],
  Gm6: [
    [0, 8, 0, 8, 0],
    [1, 0, 0, 0, 1],
    [2, 0, 2, 0, 0],
  ],
  Gadd9: [
    [0, 8, 0, 0, 0],
    [2, 0, 1, 0, 1],
    [5, 0, 0, 4, 0],
  ],
  Gm9: [
    [1, 0, 0, 0, 1],
    [2, 2, 3, 0, 0],
    [3, 0, 0, 4, 0],
  ],
  G9: [
    [1, 0, 0, 1, 0],
    [2, 2, 3, 0, 4],
  ],
  Gsus2: [
    [0, 8, 0, 0, 8],
    [2, 0, 1, 0, 0],
    [3, 0, 0, 2, 0],
  ],
  Gsus4: [
    [2, 0, 1, 0, 0],
    [3, 0, 0, 3, 4],
  ],
  G7sus2: [
    [0, 8, 0, 0, 8],
    [1, 0, 0, 1, 0],
    [2, 0, 2, 0, 0],
  ],
  G7sus4: [
    [0, 8, 0, 0, 0],
    [1, 0, 0, 1, 0],
    [2, 0, 2, 0, 0],
    [3, 0, 0, 0, 3],
  ],
  Ab: [
    [3, 0, 1, 0, 2],
    [4, 0, 0, 3, 0],
  ],
  "Ab^2": [
    [3, 1, 1, 1, 1],
    [4, 0, 0, 2, 0],
    [5, 3, 0, 0, 0],
  ],
  Abm: [
    [2, 0, 0, 0, 1],
    [3, 0, 2, 0, 0],
    [4, 0, 0, 3, 0],
  ],
  "Abm^2": [
    [2, 1, 1, 1, 1],
    [3, 0, 2, 0, 0],
    [4, 3, 0, 4, 0],
  ],
  Abaug: [
    [0, 0, 8, 8, 0],
    [1, 1, 0, 0, 0],
    [3, 0, 0, 0, 4],
  ],
  Abdim: [
    [1, 1, 0, 2, 0],
    [2, 0, 3, 0, 4],
  ],
  Ab7: [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 2, 0],
    [3, 0, 3, 0, 4],
  ],
  Abm7: [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 3, 2],
    [3, 0, 4, 0, 0],
  ],
  Abmaj7: [
    [1, 1, 0, 0, 0],
    [3, 0, 2, 3, 4],
  ],
  Ab6: [
    [1, 1, 1, 1, 1],
    [3, 0, 3, 0, 4],
  ],
  Abm6: [
    [4, 1, 1, 1, 1],
    [5, 0, 2, 0, 0],
    [6, 0, 0, 0, 3],
  ],
  Abadd9: [
    [3, 1, 1, 1, 1],
    [4, 0, 0, 2, 0],
  ],
  Abm9: [
    [2, 1, 1, 1, 1],
    [3, 2, 3, 0, 0],
    [4, 0, 0, 4, 0],
  ],
  Ab9: [
    [1, 0, 0, 1, 0],
    [2, 2, 3, 0, 4],
  ],
  Absus2: [
    [1, 1, 1, 1, 1],
    [3, 0, 3, 0, 0],
    [4, 0, 0, 4, 0],
  ],
  Absus4: [
    [1, 1, 1, 1, 1],
    [3, 0, 3, 3, 3],
    [4, 0, 0, 4, 4],
  ],
  Ab7sus4: [
    [1, 1, 1, 1, 1],
    [2, 0, 0, 2, 0],
    [3, 0, 3, 0, 0],
    [4, 0, 0, 0, 4],
  ],
  NC: [[0, 9, 9, 9, 9]],
};

// normalise the dictionary
const nDictionary: Dictionary = {};
for (const key in dictionary) {
  nDictionary[key] = normaliseChordDef(dictionary[key]);
}

const nMappings = [
  ["N/C", "NC"],
  ["A#", "Bb"],
  ["C#", "Db"],
  ["D#", "Eb"],
  ["F#", "Gb"],
  ["G#", "Ab"],
  ["+", "aug"],
];

function normaliseName(chord: string): string {
  nMappings.forEach((mapping) => {
    chord = chord.replace(mapping[0], mapping[1]);
  });
  return chord;
}

function rNormaliseName(chord: string): string {
  nMappings.forEach((mapping) => {
    chord = chord.replace(mapping[1], mapping[0]);
  });
  return chord;
}

export function lookUpChordDef(chord: string): ChordDefinition | undefined {
  // replace the sharp version with flatten version
  // and deal with the + notation for augmented chords
  return dictionary[normaliseName(chord)];
}

export function nameIsAChord(text: string) {
  return lookUpChordDef(text) !== undefined;
}

export function removeBarres(chordDef: ChordDefinition): ChordDefinition {
  // assume no duplicate fret numbers, and that every line has a fret number
  // order by fret number
  const nChordDef = chordDef.sort((a, b) => a[0] - b[0]);
  for (let i = 1; i < 5; ++i) {
    let alreadySet = false;
    for (let j = nChordDef.length - 1; j >= 0; --j) {
      if (alreadySet) {
        nChordDef[j][i] = 0;
      } else if (nChordDef[j][i] !== 0) {
        alreadySet = true;
      }
    }
  }
  return nChordDef;
}

export function deepCopy(chordDef: ChordDefinition): ChordDefinition {
  const ret: ChordDefinition = [];
  chordDef.forEach((f) => {
    ret.push([f[0], f[1], f[2], f[3], f[4]]);
  });
  return ret;
}

export function normaliseChordDef(chordDef: ChordDefinition): ChordDefinition {
  // treat no 0 and 9 as the same
  // treat no 1,2,3,4 and 8 as the same
  let nChordDef = deepCopy(chordDef).map((fret) => {
    return fret.map((uke_str, i) => {
      // this is the fret number
      if (i === 0) {
        return uke_str;
      }
      return uke_str === 9 || uke_str === 0 ? 0 : 8;
    });
  });
  // remove any empty string lines
  nChordDef = nChordDef.filter((fret) => {
    return fret.some((uke_str, i) => i !== 0 && uke_str !== 0);
  });
  return removeBarres(nChordDef);
}

export function chordDefEqual(a: ChordDefinition, b: ChordDefinition): boolean {
  if (a.length !== b.length) {
    return false;
  }
  for (let i = 0; i < a.length; ++i) {
    // this should always been true
    if (a[i].length !== b[i].length) {
      return false;
    }
    for (let j = 0; j < a[i].length; ++j) {
      if (a[i][j] !== b[i][j]) {
        return false;
      }
    }
  }

  return true;
}

export function lookUpNames(chordDef: ChordDefinition): Array<string> {
  // iterate over normalised chords to find matches - normalise on the way
  const ret: Array<string> = [];
  const nChordRef = normaliseChordDef(chordDef);
  for (const key in nDictionary) {
    if (chordDefEqual(nChordRef, nDictionary[key])) {
      // also check for normalised versions (e.g. A# to Bb)
      const altKey = rNormaliseName(key);
      if (altKey !== key) {
        ret.push(altKey);
      }
      ret.push(key);
    }
  }
  return ret;
}

export function makeLeftHanded(chordDef: ChordDefinition) {
  const ret: ChordDefinition = [];
  chordDef.forEach((fret) => {
    const rFret = [];
    rFret.push(fret[0]);
    for (let i = fret.length - 1; i >= 1; --i) {
      rFret.push(fret[i]);
    }
    ret.push(rFret);
  });
  return ret;
}

export type MultiChordDefinition = {
  chordDef: ChordDefinition;
  names: Array<string>;
};
const uDictionary: Array<MultiChordDefinition> = [];
export function uniqueChordDefs(): Array<MultiChordDefinition> {
  // creating this is n^2 expensive so cache the result
  if (uDictionary.length > 0) {
    return uDictionary;
  }

  // find only the unique chords defs (same chord could have different names)
  const chordsFound: Set<string> = new Set<string>();
  for (const key in nDictionary) {
    const names = lookUpNames(nDictionary[key]);
    if (!chordsFound.has(names[0])) {
      uDictionary.push({ chordDef: nDictionary[key], names: names });
      names.forEach((name) => chordsFound.add(name));
    }
  }

  return uDictionary;
}

export type ChordName = {
  name: string;
  voicing?: string;
};
export function parseChordName(chordName: string): ChordName {
  let voicing = undefined;
  const chordParts = chordName.split("^");
  if (chordParts.length == 2) {
    voicing = chordParts[1];
  }
  return {
    name: chordParts[0],
    voicing,
  };
}

export function offsetNoteBy(note: string, offset: number) {
  const notes = [
    "A",
    "A#",
    "B",
    "C",
    "C#",
    "D",
    "D#",
    "E",
    "F",
    "F#",
    "G",
    "G#",
  ];
  note = note
    .replace("Bb", "A#")
    .replace("Db", "C#")
    .replace("Eb", "D#")
    .replace("Gb", "F#")
    .replace("Ab", "G#");
  offset = offset % 12;
  let i = notes.findIndex((n) => n === note);
  if (i >= 0) {
    i += offset;
    if (i >= notes.length) {
      i -= 12;
    }
    return notes[i];
  } else {
    throw new Error("Invalid Note " + note);
  }
}
