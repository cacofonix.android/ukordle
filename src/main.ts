import { createApp } from "vue";
import App from "./App.vue";
import { globalCookiesConfig } from "vue3-cookies";

globalCookiesConfig({
  expireTimes: Infinity,
  path: "/",
  domain: "",
  secure: true,
  sameSite: "None",
});

const app = createApp(App);

app.mount("#app");
